import numpy as np
from simulation import tiroParabolicoConViento

if __name__ == '__main__':
    tiro = tiroParabolicoConViento(-10, np.pi/3, 5, 0, 0, -3)
    tiro.plot_parabolico()