import numpy as np
import matplotlib.pyplot as plt

class tiroParabolico:
    def __init__(self,theta,v0,g,r0):
        '''
        Clase que simula un tiro parabólico dadas las condiciones iniciales
        INPUT:
        theta: ángulo de disparo en grados.
        v0: magnitud de la velocidad inicial en m/s.
        g: magnitud de la aceleración gravitacional en m/s².
        r0: posición inicial en m, arreglo de numpy de la forma [x_0,y_0] con y_0>0.

        ATRIBUTOS:
        theta0: ángulo de disparo en radianes.
        v0: magnitud de la velocidad inicial en m/s.
        g: magnitud de la aceleración gravitacional en m/s².
        r0: posición inicial en m, arreglo de numpy de la forma [x_0,y_0].
        tV: tiempo de vuelo de la partícula en s.
        '''
        if r0[1] < 0:
            raise Exception (f'La altura inicial debe ser positiva. Usted ingresó {r0[1]}.')
        if g < 0:
            raise Exception (f'La magnitud de la aceleración gravitacional debe ser positiva. Usted ingresó {g}.')
        self.theta0 = np.deg2rad(theta)
        self.v0 = v0
        self.g = g
        self.r0 = r0

        # x = x0 + v0cos(theta0)
        # y = y0 + v0sin(theta0)t - 1/2gt²

        # t_max = t(y=0) -> t_max = (v0sin(theta0) +- (v0²sin²(theta0)+2gy0)^0.5)/g

        discr = self.v0**2*(np.sin(self.theta0))**2 + 2*self.g*self.r0[1]
        tv_pos = (self.v0*np.sin(self.theta0) + discr**0.5)/self.g
        tv_neg = (self.v0*np.sin(self.theta0) - discr**0.5)/self.g

        self.tV = max([tv_pos,tv_neg])

    def velocidad(self,n=100):
        '''
        Calcula la velocidad de la partícula en un intervalo (0,tV) con n=100 pasos.

        INPUT:
        n: número de pasos (opcional, 100 por defecto).

        OUTPUT:
        vs: arreglo con n vectores de la forma [v_x,v_y]
        '''
        ts = np.linspace(0,self.tV,n)
        vs = np.zeros((n,2))
        v = lambda t: 5.*np.array([np.cos(np.pi/6),
                                  np.sin(np.pi/6)])+ np.array([0.,-9.8])*t
        for i,t in enumerate(ts):
            vs[i] = v(t)

        return vs
    
    def posicion(self,n=100):
        '''
        Calcula la posición de la partícula en un intervalo (0,tV) con n=100 pasos.

        INPUT:
        n: número de pasos (opcional, 100 por defecto).

        OUTPUT:
        rs: arreglo con n vectores de la forma [x,y]
        '''

        r = lambda t: self.r0 + self.v0*np.array([np.cos(self.theta0),
                                                  np.sin(self.theta0)])*t + 1/2*np.array([0,-self.g])*t**2
        ts = np.linspace(0,self.tV,n)
        rs = np.zeros((n,2))
        for i,t in enumerate(ts):
            rs[i] = r(t)

        return rs
    
    def trayectoria(self,n=100):
        '''
        Dibuja la trayectoria de la partícula desde el momento del lanzamiento hasta que
        cae otra vez al suelo.

        INPUT:
        n: número de pasos para el cálculo de la posición (opcional, 100 por defecto)

        OUTPUT:
        Gráfica de la trayectoria.
        
        '''
        Rs = self.posicion(n=100)
        plt.plot(Rs[:,0],Rs[:,1],'r')
        plt.title('Trayectoria')
        plt.xlabel(r'$x(t)$ [m]')
        plt.ylabel(r'$y(t)$ [m]')
        plt.grid()
        plt.savefig('tray.png')
        plt.show()







        
        