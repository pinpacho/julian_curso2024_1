# Parámetros de la EDO
import numpy as np
import matplotlib.pyplot as plt


class RKG:
    """
    Clase para calcular las soluciones de una EDO de grado 1 usando el método de Runge-Kutta generalizado.

    Atributos:
    - f: La función que define la derivada.
    - initial_conditions: lista que contiene las condiciones iniciales [x0, y0].
    - x_end: El valor de x hasta donde se calcularán las soluciones.
    - step_size: El tamaño del paso para avanzar en la solución.
    - order: El orden del método de Runge-Kutta.
    - constants: Las constantes que definen el método de Runge-Kutta.
    """

    def __init__(self, f, initial_conditions, x_end, step_size, order, constants):
        self.f = f
        self.initial_conditions = initial_conditions
        self.x_end = x_end
        self.step_size = step_size
        self.order = order
        self.constants = constants
        self.num_steps = int((x_end - initial_conditions[0]) / step_size) 

    def ks(self, xi, yi, h):
        """
        Calcula los factores k en cada paso necesarios para llegar a la solución por Runge-Kutta generalizado.

        Args:
        - xi: El valor actual de x.
        - yi: El valor actual de y.
        - h: El tamaño del paso.

        Returns:
        Una lista con los factores k.
        """
        ks = []
        c = self.constants
        k = [0] * self.order
        for j in range(self.order):
            for i in range(j):
                k[j] += c[j][i] * k[i]
            k[j] = h /self.order * self.f(xi + c[j][self.order - 1] * h, yi + k[j]  )
        for j in range(self.order):
            ks.append(k[j])
        return ks

       
    def soluciones(self):
        """
        Calcula las soluciones en cada xi.

        Returns:
        Un arreglo que contiene un valor de x y su correspondiente valor de y.
        """
        xi = self.initial_conditions[0]
        yi = self.initial_conditions[1]
        h = self.step_size 
        sol = [(xi, yi)]
        for _ in range(1, self.num_steps):
            ks = self.ks(xi, yi, h)
            for j in range(self.order):
                yi += ks[j]
            xi += h
            sol.append((xi, yi))
        return sol

class RKGA(RKG):
    """
    Clase para calcular las soluciones de un conjunto de ecuaciones diferenciales utilizando el método de Runge-Kutta 4,
    heredando de la clase RKG.

    Atributos heredados de RKG:
    - f: La función que define las derivadas.
    - initial_conditions: lista que contiene las condiciones iniciales [t0, L0=[x0,y0,z0]].
    - x_end: El valor de t hasta donde se calcularán las soluciones.
    - step_size: El tamaño del paso para avanzar en la solución.
    - order: El orden del método de Runge-Kutta.
    - constants: Las constantes que definen el método de Runge-Kutta.
    """

    def __init__(self, f, initial_conditions, x_end, step_size, order, constants):
        """
        Inicializa una instancia de la clase RKGA.

        """
        super().__init__(f, initial_conditions, x_end, step_size, order, constants)

    def soluciones(self):
        """
        Calcula la solución de un conjunto de ecuaciones diferenciales utilizando el método de Runge-Kutta 4.

        Returns:
        Un arreglo de la forma t,L, donde t es una lista de valores de tiempo y L es una matriz de valores de las soluciones en esos tiempos.
        """
        h = self.step_size

        t = np.arange(self.initial_conditions[0], self.x_end + h, h) 
        L = np.zeros((len(t), len(self.initial_conditions[1])))
        L[0] = self.initial_conditions[1]

        for i in range(0, len(t) - 1):
            k1 = self.f(t[i], L[i])
            k2 = self.f(t[i] + h * 0.5, L[i] + k1 * h * 0.5)
            k3 = self.f(t[i] + h * 0.5, L[i] + k2 * h * 0.5)
            k4 = self.f(t[i] + h, L[i] + k3 * h)

            L[i + 1] = L[i] + h * (k1 + 2 * k2 + 2 * k3 + k4) / 6

        return t, L


if __name__ == '__main__':
    import numpy as np
    import matplotlib.pyplot as plt

    def f(x, y):
        return x - y

    def exact_solution(x):
        return 2 * np.exp(-x) + x - 1

    # Definición de constantes del método de Runge-Kutta 4
    order = 4
    constants = [[0, 0, 0, 0], [0.5, 0, 0, 0], [0, 0.5, 0, 0], [0, 0, 1, 0]]

    # Parámetros de la EDO
    initial_conditions = (0.0, 1.0)
    x_end = 1
    step_size = 0.00001  

    # Resolución de la EDO usando Runge-Kutta
    solver = RKG(f, initial_conditions, x_end, step_size, order, constants)
    sol = solver.soluciones()

    # Extracción de los valores de x e y de la solución numérica
    x_values = np.array([point[0] for point in sol])
    y_values = np.array([point[1] for point in sol])

    # Solución exacta para comparación
    exact_x = np.linspace(0, x_end, 100)
    exact_y = exact_solution(exact_x)

    # Cálculo del error entre la solución numérica y la solución exacta
    error = np.abs(exact_solution(x_values) - y_values)
    

    # Gráfico de la solución numérica y exacta
    plt.figure(figsize=(12, 5))

    # Gráfico de la solución numérica y exacta
    plt.plot(x_values, y_values, label='Solución numérica (RK4)', color='blue')
    plt.plot(exact_x, exact_y, label='Solución exacta', linestyle='--', color='red')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Comparación de la solución numérica y exacta')
    plt.legend()
    plt.grid(True)
    plt.show()
    plt.close()

    # Gráfico de la convergencia
    plt.figure(figsize=(8, 5))
    plt.semilogy(x_values, error, label='Error de convergencia', color='green')
    plt.xlabel('x')
    plt.ylabel('Error')
    plt.title('Convergencia de la solución numérica hacia la solución exacta')
    plt.legend()
    plt.grid(True)
    plt.show()
    plt.close()












    
        

    

    
        


    


    

        
    
    
    